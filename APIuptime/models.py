from django.db import models


class Request(models.Model):
    TrackingCode = models.CharField(max_length=100, unique=True)
    ServiceName = models.CharField(max_length=50)
    Request = models.TextField()
    Response = models.TextField()

    class Meta:
        verbose_name = "request"
        verbose_name_plural = "requests"

    def __str__(self):
        return self.TrackingCode
