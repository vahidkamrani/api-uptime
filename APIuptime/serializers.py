from rest_framework import serializers
from .models import Request


class RequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Request
        fields = ['TrackingCode', 'ServiceName', 'Request', 'Response']


class GetRequestLogSerializer(serializers.Serializer):
    TrackingCode = serializers.CharField(max_length=100, required=True)


class GetPackageUsageSerializer(serializers.Serializer):
    TrackingCode = serializers.CharField(max_length=100, required=True)
    ServiceNumber = serializers.CharField(max_length=11, required=True)
    FromDate = serializers.DateTimeField(required=False)
    ToDate = serializers.DateTimeField(required=False)
    PageNumber = serializers.CharField(max_length=11, required=False)



class GetPackageUsageDetailsSerializer(serializers.Serializer):
    TrackingCode = serializers.CharField(max_length=100, required=True)
    ServiceNumber = serializers.CharField(max_length=11, required=True)
    FromDate = serializers.DateTimeField(required=False)
    ToDate = serializers.DateTimeField(required=False)
    PackageId = serializers.CharField(max_length=30, required=True)
    PageNumber = serializers.CharField(max_length=11, required=False)


class GetPrePaidBillInfoSerializer(serializers.Serializer):
    TrackingCode = serializers.CharField(max_length=100, required=True)
    ServiceNumber = serializers.CharField(max_length=11, required=True)
    FromDate = serializers.DateTimeField(required=False)
    ToDate = serializers.DateTimeField(required=False)
    PageNumber = serializers.CharField(max_length=11, required=False)


class SearchOwnerSerializer(serializers.Serializer):
    TrackingCode = serializers.CharField(max_length=100, required=True)
    ServiceNumber = serializers.CharField(max_length=11, required=True)