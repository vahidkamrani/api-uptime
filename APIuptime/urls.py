from django import views
from django.urls import path
from .api_view import GetAllRequestlView, CheckHealthView, GetRequestLogView, GetPackageUsageView, GetPackageUsageDetailsView, GetCallUsageView, GetPrePaidBillInfoView, GetPostPaidBillInfoView, SearchOwnerView, GetSuspentionHistoryView



app_name = 'APIuptime'
urlpatterns = [
    path('GetAllRequests', GetAllRequestlView.as_view(), name='GetAllRequests'),
    path('CheckHealth', CheckHealthView.as_view(), name='CheckHealth'),
    path('GetRequestLog', GetRequestLogView.as_view(), name='GetRequestLog'),
    path('GetPackageUsage', GetPackageUsageView.as_view(), name='GetPackageUsage'),
    path('GetPackageUsageDetails', GetPackageUsageDetailsView.as_view(), name='GetPackageUsageDetails'),
    path('GetCallUsage', GetCallUsageView.as_view(), name='GetCallUsage'),
    path('GetPrePaidBillInfo', GetPrePaidBillInfoView.as_view(), name='GetPrePaidBillInfo'),
    path('GetPostPaidBillInfo', GetPostPaidBillInfoView.as_view(), name='GetPostPaidBillInfo'),
    path('SearchOwner', SearchOwnerView.as_view(), name='SearchOwner'),
    path('GetSuspentionHistory', GetSuspentionHistoryView.as_view(), name='GetSuspentionHistory'),
]
