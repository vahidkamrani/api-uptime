import requests
import datetime
import json
from django.core.exceptions import ObjectDoesNotExist
from persiantools.jdatetime import JalaliDateTime
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import GetRequestLogSerializer, RequestSerializer, GetPackageUsageSerializer, GetPackageUsageDetailsSerializer, GetPrePaidBillInfoSerializer, SearchOwnerSerializer
from .models import Request

token = "U53kBXR9maxfrz3nkNQu"


class GetAllRequestlView(APIView):
    def get(self, request):
        ser_data = Request.objects.all()
        result = RequestSerializer(instance=ser_data, many=True)
        return Response(result.data)


class CheckHealthView(APIView):
    def post(self, request):           
        response, result = {}, {}
        DateTime = str(JalaliDateTime.now().strftime("%Y/%m/%d %H:%M:%S"))
        response['HasError'] = False
        response['Message'] = "عملیات با موفقیت انجام شد"
        result['DateTime'] = DateTime
        result['Count'] = Request.objects.all().count()
        response['Result'] = result
        return Response(response, status=status.HTTP_200_OK)


class GetRequestLogView(APIView):
    serializer_class = GetRequestLogSerializer
    def post(self, request):
        ser_data = GetRequestLogSerializer(data=request.data)

        if ser_data.is_valid():
            response = {}
            trackingCode = ser_data.validated_data['TrackingCode']
            try:
                req = Request.objects.get(TrackingCode=trackingCode)
                result = RequestSerializer(req)
                response['HasError'] = False
                response['Message'] = "عملیات با موفقیت انجام شد"
                response['Result'] = result.data
                return Response(response, status=status.HTTP_200_OK)
            except ObjectDoesNotExist:
                return Response({"message": "بر اساس ورودی های ارسالی نتیجه ای یافت نشد."}, status=status.HTTP_200_OK)


class GetPackageUsageView(APIView):
    serializer_class = GetPackageUsageSerializer

    def post(self, request):
        global token
        ser_data = GetPackageUsageSerializer(data=request.data)

        def convert_datetime(dt, extra=0):
            time_temp = dt.replace("-", "/")
            convert_gregorian_to_format = time_temp.replace("T", " ")
            jalali_format = str(JalaliDateTime.to_jalali(datetime.datetime.strptime(convert_gregorian_to_format, "%Y/%m/%d %H:%M:%S")) + datetime.timedelta(days=extra))
            final_format = jalali_format.replace("-", "/")
            return final_format

        def service_type(service):
            if service == 2:
                return "وایرلس"
            elif service == 5:
                return "پهنای باند"
            elif service == 1:
                return "ADSL"
            else:
                return "Dg3"

        if ser_data.is_valid():
            trackingCode = ser_data.validated_data['TrackingCode']
            internetUsername = ser_data.validated_data['ServiceNumber']
            fromDate = ser_data.validated_data['FromDate']
            toDate = ser_data.validated_data['ToDate']
            pageNumber = ser_data.validated_data['PageNumber']

            parameters = {
                'token': token,
                'internetUsername': internetUsername,
                'fDate': fromDate,
                'tDate': toDate,
                # 'PageNumber': pageNumber,
            }

            result = requests.get('https://my.didi.ir/crmAPIServiceUsages', params=parameters).text
            result_services = requests.get('https://my.didi.ir/crmAPISeviceBilling', params=parameters).text

            if result and result_services:
                response = {}
                requestInfo = dict(ser_data.validated_data)
                json_data = json.loads(result)
                json_data_services = json.loads(result_services)
                if 'error' in json_data or 'error' in json_data_services:
                    return Response({"message": "اطلالات مشترک مورد نظو در سمت اپراتور موجود نمی باشد"}, status=status.HTTP_200_OK)
                response['HasError'] = False
                response['Message'] = "عملیات با موفقیت انجام شد"
                response['Result'] = {}
                response['Result']['Packages'] = []
                temp_services, consume = 0, 0
                for product in json_data['products']:
                    temp_services += product['trafic']
                    service = {}
                    service["id"] = product['_id']
                    service["Title"] = product['generatedTitle']
                    service["Volume"] = str(product['trafic'] * 1024) + " MB" 
                    service["ConnectionType"] = service_type(int(product['forService'][0]))
                    service["Type"] = ""
                    service["Period"] = product["durationF"]
                    service["PaymentType"] = "PrePaid"
                    for order in json_data_services['orders']:
                        jalali_format = convert_datetime(order['created'][:-5])
                        
                        if request.data['FromDate'] <= jalali_format <= request.data['ToDate']:
                            service["PurchaseDateTime"] = convert_datetime(order['items'][0]['operation']['created'][:-5])                           
                            service["AutoActivate"] = "خیر"
                            service["ActivateDateTime"] = convert_datetime(order['items'][0]['operation']['doneDate'][:-5])  
                            service["StartDateTime"] = convert_datetime(order['items'][0]['operation']['doneDate'][:-5])  
                            service["EndDateTime"] = convert_datetime(order['items'][0]['operation']['doneDate'][:-5], order['items'][0]['product']['duration'])  
                            service["FirstDateTime"] = ""
                            service["LastDateTime"] = ""
                            service["Duration"] = ""
                        service["Status"] = "فعال" if product['status']==1 else "غیر فعال"
                        service["TotalTraffic"] = str(product['trafic'] * 1024) + " MB" 
                    UploadTraffic = 0
                    DownloadTraffic = 0
                    for connectionLog in json_data['connectionLogs']:
                        if connectionLog["group_name"] == product["didiIBSGroup"]:
                            UploadTraffic += float(connectionLog["bytes_out"]) / 1047576
                            DownloadTraffic += float(connectionLog["bytes_in"]) / 1047576
                    
                    service["UploadTraffic"] = str(UploadTraffic) + " MB"
                    service["DownloadTraffic"] = str(DownloadTraffic) + " MB"
                    service["RemainTraffic"] = str((product['trafic'] * 1024) - (DownloadTraffic + UploadTraffic)) + " MB"

                    response['Result']['Packages'].append(service)

                response['Result']['TotalSize'] = str(temp_services * 1024) + " MB"
                for connectionLog in json_data['connectionLogs']:
                    consume += int(connectionLog['bytes_in']) + int(connectionLog['bytes_out'])
                
                response['Result']['TotalUsage'] = str(consume / 1047576) + " MB"

                response['Result']['TotalRemain'] = str(((temp_services * 1024)-consume / 1047576)) + " MB"
                response['Result']['TotalPage'] = 0

                Request.objects.create(TrackingCode=trackingCode, ServiceName="GetPackageUsage", Request=requestInfo, Response=response)

                return Response(response, status=status.HTTP_200_OK)

        else:
            return Response(ser_data.errors, status=status.HTTP_200_OK)


class GetPackageUsageDetailsView(APIView):
    serializer_class = GetPackageUsageDetailsSerializer
    def post(self, request):
        global token
        ser_data = GetPackageUsageDetailsSerializer(data=request.data)

        def service_type(service):
            if service == "Intranet":
                return "نیم بها"
            elif service == "Free":
                return "رایگان"
            else:
                return "بین الملل"

        if ser_data.is_valid():
            trackingCode = ser_data.validated_data['TrackingCode']
            internetUsername = ser_data.validated_data['ServiceNumber']
            fromDate = ser_data.validated_data['FromDate']
            toDate = ser_data.validated_data['ToDate']
            packageId = ser_data.validated_data['PackageId']
            pageNumber = ser_data.validated_data['PageNumber']

            parameters = {
                'token': token,
                'internetUsername': internetUsername,
                'fDate': fromDate,
                'tDate': toDate,
            }

            result = requests.get('https://my.didi.ir/crmAPIServiceUsages', params=parameters).text

            if result:
                response = {}
                requestInfo = dict(ser_data.validated_data)
                
                json_data = json.loads(result)
                if 'error' in json_data:
                    return Response({"message": "اطلالات مشترک مورد نظو در سمت اپراتور موجود نمی باشد"}, status=status.HTTP_200_OK)
                response['HasError'] = False
                response['Message'] = "عملیات با موفقیت انجام شد"
                response['Result'] = {}
                response['Result']['PackageDetails'] = []
                for product in json_data['products']:
                    if product['_id'] == packageId:
                        service = {}
                        service["type"] = service_type(json_data['connectionLogs'][0]['service_type'])
                        service["TotalTotalTraffic"] = str(product['trafic'] * 1024) + " MB"

                        UploadTraffic = 0
                        DownloadTraffic = 0

                        for connectionLog in json_data['connectionLogs']:
                            if connectionLog["group_name"] == product["didiIBSGroup"]:
                                UploadTraffic += float(connectionLog["bytes_out"]) / 1047576
                                DownloadTraffic += float(connectionLog["bytes_in"]) / 1047576
                    
                        service["UploadTraffic"] = str(UploadTraffic) + " MB"
                        service["DownloadTraffic"] = str(DownloadTraffic) + " MB"
                        response['Result']['PackageDetails'].append(service)

                response['Result']['TotalPage'] = 0

                Request.objects.create(TrackingCode=trackingCode, ServiceName="GetPackageUsage", Request=requestInfo, Response=response)

                return Response(response, status=status.HTTP_200_OK)

        else:
            return Response(ser_data.errors, status=status.HTTP_200_OK)


class GetCallUsageView(APIView):
    def post(self, request):
        response = {}
        response['HasError'] = False
        response['Message'] = "عملیات با موفقیت انجام شد"
        response['Result'] = {}
        response['Result']['message'] = "این سرویس در حال حاظر از سمت اپراتور ارائه نمی شود"
        return Response(response)


class GetPrePaidBillInfoView(APIView):
    serializer_class = GetPrePaidBillInfoSerializer
    def post(self, request):
        global token
        ser_data = GetPrePaidBillInfoSerializer(data=request.data)

        def convert_datetime(dt, extra=0):
            time_temp = dt.replace("-", "/")
            convert_gregorian_to_format = time_temp.replace("T", " ")
            jalali_format = str(JalaliDateTime.to_jalali(datetime.datetime.strptime(convert_gregorian_to_format, "%Y/%m/%d %H:%M:%S")) + datetime.timedelta(days=extra))
            final_format = jalali_format.replace("-", "/")
            return final_format

        if ser_data.is_valid():
            trackingCode = ser_data.validated_data['TrackingCode']
            internetUsername = ser_data.validated_data['ServiceNumber']
            fromDate = ser_data.validated_data['FromDate']
            toDate = ser_data.validated_data['ToDate']
            pageNumber = ser_data.validated_data['PageNumber']

            parameters = {
                'token': token,
                'internetUsername': internetUsername,
                # 'PageNumber': pageNumber,
            }

            result = requests.get('https://my.didi.ir/crmAPISeviceBilling', params=parameters).text

            if result:
                response = {}
                requestInfo = dict(ser_data.validated_data)
                
                json_data = json.loads(result)

                if 'error' in json_data:
                    return Response({"message": "اطلالات مشترک مورد نظو در سمت اپراتور موجود نمی باشد"}, status=status.HTTP_200_OK)
                response['HasError'] = False
                response['Message'] = "عملیات با موفقیت انجام شد"
                response['Result'] = {}
                response['Result']['TotalPage'] = 0
                response['Result']['Billings'] = []
                for order in json_data['orders']:
                    final_format = convert_datetime(order['created'][:-5])

                    if request.data['FromDate'] <= final_format <= request.data['ToDate']:
                        print(final_format)
                        service = {}
                        service['Type'] = "بسته اینترنت"
                        service['Title'] = order['items'][0]['product']['generatedTitle']
                        service['Price'] = str(order['price']) + " R"
                        service['Period'] = ""
                        service['Gateway'] = order['paymentType']
                        service['PaidAmount'] = str(order['Transaction'][0]['price']) + " R"
                        service['PaidDateTime'] = convert_datetime(order['Transaction'][0]['returnFromBank'][:-5])
                        service['Status'] = "موفق"
                        service['Details'] = []
                        tax = {}
                        tax['Type'] = "مالیات و عوارض"
                        tax['Amount'] = str((order['price'] * 9) / 100) + " R"
                        service['Details'].append(tax)
                        item = {}
                        item['Type'] = "نرخ بسته"
                        item['Amount'] = str(order['items'][0]['product']['price']) + " R"
                        service['Details'].append(item)
                    
                        response['Result']['Billings'].append(service)
                        

                Request.objects.create(TrackingCode=trackingCode, ServiceName="GetPackageUsage", Request=requestInfo, Response=response)

                return Response(response, status=status.HTTP_200_OK)

        else:
            return Response(ser_data.errors, status=status.HTTP_200_OK)


class GetPostPaidBillInfoView(APIView):
    def post(self, request):
        response = {}
        response['HasError'] = False
        response['Message'] = "عملیات با موفقیت انجام شد"
        response['Result'] = {}
        response['Result']['message'] = "این سرویس در حال حاظر از سمت اپراتور ارائه نمی شود"
        return Response(response)


class SearchOwnerView(APIView):
    serializer_class = SearchOwnerSerializer
    def post(self, request):
        global token
        ser_data = SearchOwnerSerializer(data=request.data)

        if ser_data.is_valid():
            trackingCode = ser_data.validated_data['TrackingCode']
            internetUsername = ser_data.validated_data['ServiceNumber']

            parameters = {
                'token': token,
                'internetUsername': internetUsername,
            }

            result = requests.get('https://my.didi.ir/serviceInfo', params=parameters).text

            if result:
                response = {}
                requestInfo = dict(ser_data.validated_data)
                json_data = json.loads(result)
                if 'error' in json_data:
                    return Response({"message": "اطلالات مشترک مورد نظو در سمت اپراتور موجود نمی باشد"}, status=status.HTTP_200_OK)
                response['HasError'] = False
                response['Message'] = "عملیات با موفقیت انجام شد"
                response['Result'] = {}
                response['Result']['MaskedNationalCode'] = json_data['serviceObj']['attrs']['custom_field_melli_code'][:3] + '*' * 5 + json_data['serviceObj']['attrs']['custom_field_melli_code'][-3:]
                response['Result']['Date'] = str(JalaliDateTime.to_jalali(datetime.datetime.strptime(json_data['serviceObj']['basic_info']['creation_date'].replace("-", "/"), "%Y/%m/%d %H:%M:%S"))).replace("-", "/")

                Request.objects.create(TrackingCode=trackingCode, ServiceName="GetPackageUsage", Request=requestInfo, Response=response)

                return Response(response, status=status.HTTP_200_OK)

        else:
            return Response(ser_data.errors, status=status.HTTP_200_OK)


class GetSuspentionHistoryView(APIView):
    def post(self, request):
        response = {}
        response['HasError'] = False
        response['Message'] = "عملیات با موفقیت انجام شد"
        response['Result'] = {}
        response['Result']['message'] = "این سرویس در حال حاظر از سمت اپراتور ارائه نمی شود"
        return Response(response)