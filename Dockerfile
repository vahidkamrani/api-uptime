FROM python:latest

WORKDIR /code

COPY packages.txt /code/

RUN pip install -U pip
RUN pip install -r packages.txt

COPY . /code/

CMD ["gunicorn", "Uptime.wsgi", ":8000"]
